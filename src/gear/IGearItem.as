package gear
{
	

	public interface IGearItem
	{
		public function get id:String;
		public function get type:String;
		
		public function toString:String;
		public function destroy:void;
	}
}