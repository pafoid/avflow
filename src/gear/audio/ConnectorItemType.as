package gear.audio
{
	public class ConnectorItemType
	{
		public static const AES3_OVER_XLR:String 			= "analog";
		public static const AES3_OVER_DB_25:String 			= "analog";
		public static const OPTICAL_TO_SLINK:String 		= "analog";
		public static const OPTICAL_MADI:String 			= "analog";
		public static const SP_DIF_OVER_COAX:String 		= "analog";
		public static const AES_67_VIA_ETHERNET:String 		= "analog";
		public static const ETHERNET:String 				= "analog";

		public static const TYPES:Array = [
											AES3_OVER_XLR,
											AES3_OVER_DB_25,
											OPTICAL_TO_SLINK,
											OPTICAL_MADI,
											SP_DIF_OVER_COAX,
											AES_67_VIA_ETHERNET,
											ETHERNET
		];
	}
}