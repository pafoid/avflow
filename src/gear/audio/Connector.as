package gear.audio
{
	import gear.GearItemType;
	
	public class Connector extends AudioItem implements IAudioItem
	{
		public function Connector(id:String = null)
		{
			super(id);
			
			type = GearItemType.CONNECTOR;
		}
	}
}