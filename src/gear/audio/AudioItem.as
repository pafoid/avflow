package gear.audio
{
	import gear.GearItem;
	
	public class AudioItem extends GearItem implements IAudioItem
	{
		public function AudioItem(id:String = null)
		{
			super(id);
		}
	}
}