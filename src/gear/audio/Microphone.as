package gear.audio
{
	import gear.GearItemType;
	import gear.GearItem;
	
	public class Microphone extends GearItem implements IAudioItem
	{
		public function Microphone(id:String = null)
		{
			super(id);
			
			type = GearItemType.MICROPHONE;
		}
	}
}