package data.implementations.audio
{
	import gear.GearItem;
	
	public class Speaker extends GearItem implements IAudioItem
	{
		public function Speaker(id:String=null)
		{
			super(id);
		}
	}
}