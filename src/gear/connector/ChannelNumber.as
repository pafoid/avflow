package gear.connector
{
	import com.pafoid.display.Color;
	
	public class ChannelNumber
	{
		public static const COLORS:Array = [Color.BLACK, Color.BROWN, Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.PURPLE, Color.GREY, Color.WHITE];
		
		public static const BLACK:int 		= 0;
		public static const BROWN:int 		= 1;
		public static const RED:int 		= 2;
		public static const ORANGE:int 		= 3;
		public static const YELLOW:int 		= 4;
		public static const GREEN:int 		= 5;
		public static const BLUE:int 		= 6;
		public static const PURPLE:int 		= 7;
		public static const GREY:int 		= 8;
		public static const WHITE:int 		= 9;
	}
}