package gear.computer
{
	public class ComputerItemType
	{
		public static const MAC:String 				= "mac";
		public static const PC:String 				= "pc";
		public static const MUSIC_BOX:String 		= "musicBox";
		public static const CUSTOM:String 			= "custom";
		
		public static const TYPES:Array = [MAC, PC, MUSIC_BOX, CUSTOM];
	}
}