package gear.computer
{	
	import gear.GearItem;

	public class Computer extends GearItem implements IComputerItem
	{
		public function Computer(id:String=null, computerType:String)
		{
			super(id, computerType);
			
			this.computerType = computerType;
		}
	}
}