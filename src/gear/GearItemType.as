package gear
{
	public class GearItemType
	{
		//Audio
		public static const MICROPHONE:String 			= "microphone";
		public static const SPEAKER:String 				= "speaker";
		public static const CONSOLE:String 				= "console";
		public static const CONNECTOR:String 			= "connector";
		public static const INSTRUMENT:String 			= "instrument";
		public static const AMPLIFICATOR:String 		= "amp";
		public static const PREAMP:String 				= "preamp";
		public static const SIGNAL_PROCESSOR:String 	= "signalProcessor";
		public static const CONVERTER:String 			= "converter";
		
		//Video
		public static const CAMERA:String 				= "camera";
		public static const VIDEO_MONITOR:String 		= "videoMonitor";
		public static const TV:String 					= "TV";
		public static const PROJECTOR:String 			= "projector";
		
		//A/C
		public static const US_AC_100V:String 			= "110V US AC";
		public static const SOCAPEX:String 				= "Socapex";
		public static const POWER_CON:String 			= "PowerCon";
		public static const CAM_LOC:String 				= "CamLoc";
		
		public static const COMPUTER:String 			= "computer";
		
		public static const TYPES:Array = [
											MICROPHONE,
											SPEAKER,
											CONSOLE,
											COMPUTER,
											CONNECTOR,
											INSTRUMENT,
											AMPLIFICATOR,
											PREAMP,
											SIGNAL_PROCESSOR,
											CONVERTER,
											
											CAMERA,
											VIDEO_MONITOR,
											TV,
											PROJECTOR,
											
											US_AC_100V,
											SOCAPEX,
											POWER_CON,
											CAM_LOC,

											COMPUTER
		];
	}
}