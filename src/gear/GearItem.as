package gear
{

	public class GearItem implements IGearItem
	{
		public var id:String;
		public var type:String;
		
		public function GearItem(id:String = null)
		{
			this.id = (id != null) ? id : generateId();
		}
		
		private function generateId():String
		{
			return "new GearItem";
		}
	}
}